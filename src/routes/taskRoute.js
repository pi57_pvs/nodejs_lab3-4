const express = require('express');
const taskRouter = express.Router();
const Task = require('../models/task');

const auth = require("../middleware/auth");

require('../db/mongoose');

const bodyParser = require("body-parser");
const urlencodedParser = bodyParser.urlencoded({ extended: false });

taskRouter.use("/createTask", auth, urlencodedParser, async (req, res) => {
    const task = new Task({
        ...req.body,
        owner: req.user.id
    });
    try {
        await task.save();
        res.status(201).send(task);
    }catch (e) {
        res.status(500).send(e);
    }
});

taskRouter.get("/:id",auth, urlencodedParser, async (req, res) => {
    let taskId = req.params.id;
    const task = await Task.findById(taskId);
    await task.populate('owner').execPopulate();
    if (task.owner.id === req.user.id){
        res.send(task.owner);
    }else{
        res.status(404).send("It's not your task")
    }
});

taskRouter.use("/update/:id", auth, urlencodedParser, async (req, res) =>{
    const task = await Task.findById(req.params.id);
    await task.populate('owner').execPopulate();
    if (task.owner.id === req.user.id){
        const updates = ['description', 'completed'];
        updates.forEach((update) => task[update] = req.body[update]);
        await task.save();
        res.send(task);
    }else{
        res.status(404).send("It's not your task")
    }
});

module.exports = taskRouter;