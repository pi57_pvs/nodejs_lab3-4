const express = require('express');
const userRouter = express.Router();
const User = require('../models/user');

const auth = require("../middleware/auth");

require('../db/mongoose');

const bodyParser = require("body-parser");
const urlencodedParser = bodyParser.urlencoded({ extended: false });



userRouter.use("/save/:id", urlencodedParser, async (req, res) =>{
    const user = await User.findById(req.params.id);
    const updates = ['name', 'email', 'password', 'age'];
    updates.forEach((update) => user[update] = req.body[update]);
    await user.save();
    res.send(user);
});
userRouter.use("/create", urlencodedParser, (req, res)=>{
    let name = req.body.name;
    let age = req.body.age;
    let email = req.body.email;
    let password = req.body.password;
    const user = new User({name, age, email, password});
    try {
        user.save().then((user) => {
            res.status(200).send(user);
        }).catch((error) => {
            res.status(500).send(error);
        });
    }catch (e) {
        res.status(500).send(e);
    }

});

userRouter.use("/login", urlencodedParser, async (req, res) => {
    try{
        const user = await User.findOneByCredentials(req.body.email, req.body.password);
        const token = await user.generateAuthToken();
        console.log(user);
        res.send({user, token});
    }
    catch (e) {
        res.send(user);
    }
});

userRouter.use("/me", auth, async(req, res)=>{
    try {
        await req.user.remove();
        res.send(req.user);
    }catch (e) {
        res.status(500).send({error : "Please aunthificate"})
    }
});

userRouter.use("/logout", auth, async (req, res) => {
   try {
       req.user.tokens = req.user.tokens.filter((token) => {
           return token.token != req.token;
       });
       await req.user.save();
       res.send();
   } catch (e) {
       res.status(500).send();
   }
});
userRouter.use("/logoutAll", auth, async (req, res) => {
    try{
        req.user.token = [];
        await req.user.save();
        res.status(200).send("Logout from all devices");
    }catch (e) {
        res.status(500).send(e.message);
    }
});

userRouter.get("/myTasks",auth, async (req, res) => {
    const user = req.user;
    await user.populate('tasks').execPopulate();
    res.send(user.tasks);
});




module.exports = userRouter;