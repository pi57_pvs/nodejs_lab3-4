const isEmail = require("isemail");
const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');


let userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true
    },
    age: {
        type: Number,
        default: 0,
        validate(value){
            if(value < 0) {
                throw new Error("Age must be a positive number")
            }
        }
    },
    email: {
        type: String,
        required: true,
        trim: true,
        validate(value) {
            if(isEmail.validate(value) === false){
                throw new Error("Email is invalid")
            }
        },
        lowercase: true,
        unique: true
    },
    password: {
        type: String,
        required: true,
        trim: true,
        validate(value) {
            if(value.length < 7){
                throw new Error("Password length must be more 7")
            }
            if(value.indexOf("password") >= 0){
                throw new Error("Dont use 'password' in your Password")
            }
        }
    },
    token: [{
      token: {
          type: String,
          required: true,
      }  
    }]
});
userSchema.pre('save', async function(next) {
    const user = this;
    if(user.isModified('password')){
    user.password =await bcrypt.hash(user.password, 8);
    }
    next();
});

userSchema.statics.findOneByCredentials = async (email, password) => {
    const user = await User.findOne({email});
    try {
        if(!user){
            throw new Error('Incorrect email');
        }
        const isMatch = await bcrypt.compare(password, user.password);
        if(!isMatch){
            throw new Error('Incorrect password');
        }
        return user;
    }catch (e) {
        return e;
    }

};

userSchema.methods.generateAuthToken = async function(){
    const user = this;
    const token = jwt.sign({_id: user._id.toString()}, "qwerty");
    user.token = user.token.concat({token});
    await user.save();
    return token;
};

userSchema.methods.toJSON = function (){
    const user = this;
    const userObject = user.toObject();
    delete userObject.password;
    delete userObject.token;
    return userObject;
};

userSchema.virtual('tasks', {
   ref: "Task",
   localField: '_id',
   foreignField: 'owner'
});

const User = mongoose.model('User', userSchema);

module.exports = User;