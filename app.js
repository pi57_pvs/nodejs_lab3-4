const express = require("express");
const app = express();

const userRouter = require("./src/routes/userRoute");
const taskRouter = require("./src/routes/taskRoute");


app.use("/user", userRouter);
app.use("/task", taskRouter);


app.use(function (req, res, next) {
    res.status(404).send("Page Not Found")
});

app.listen(3000);